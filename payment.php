<?php

require 'vendor/autoload.php';

use phpseclib\Math\BigInteger;
use ZuluCrypto\StellarSdk\Server;

const MEMO_TYPE = 'MEMO_TEXT';
const XLM_PAYMENT_OPERTION_TYPE = 'Payment';

$data = [
    'SourceAccount' => 'GBOSTAO73CLG5HGB5ZCMM7OAAH5PU4WKAAC5V3YRLKWO7UVF4JJH2NIS',
    'TransactionSequenceNumber' => '1509942997549066',
    'Operations' => [
        [
            'Destination' => 'GAIH3ULLFQ4DGSECF2AR555KZ4KNDGEKN4AFI4SU2M7B43MGK3QJZNSR',
            'OperationType' => XLM_PAYMENT_OPERTION_TYPE,
            'Amount' => '0.33',
        ]
    ],
    'MemoType' => MEMO_TYPE,
    'Memo' => 'test memo',
];

/*
 * sender public key: GBOSTAO73CLG5HGB5ZCMM7OAAH5PU4WKAAC5V3YRLKWO7UVF4JJH2NIS
 * sender secret key: SDB37VJPFRUZ56WGY23JYLP54UF6OWHMTH7K4KAZBMS7EKQYTZJMSZ2D
 *
 * TransactionSequenceNumber - value of "sequence" field +1.
 * (endpoint with sequence: https://horizon-testnet.stellar.org/accounts/GBOSTAO73CLG5HGB5ZCMM7OAAH5PU4WKAAC5V3YRLKWO7UVF4JJH2NIS)
 *
 *  connecting with node:
 *  $server = Server::customNet($horizonBaseUrl, $networkPassphrase);
 */
$server = Server::testNet();

$txBuilder = $server->buildTransaction($data['SourceAccount'])
    ->setSequenceNumber(new BigInteger($data['TransactionSequenceNumber']))
    ->addLumenPayment($data['Operations'][0]['Destination'], $data['Operations'][0]['Amount']);
if (isset($data['Memo'])) {
    // setting Memo and MemoType
    $txBuilder->setTextMemo($data['Memo']);
}

$txEnvelope = $txBuilder->getTransactionEnvelope();
$txEnvelope->sign(['SDB37VJPFRUZ56WGY23JYLP54UF6OWHMTH7K4KAZBMS7EKQYTZJMSZ2D'], $server);

echo '<pre>';
print_r($server->submitB64Transaction($txEnvelope->toBase64()));
